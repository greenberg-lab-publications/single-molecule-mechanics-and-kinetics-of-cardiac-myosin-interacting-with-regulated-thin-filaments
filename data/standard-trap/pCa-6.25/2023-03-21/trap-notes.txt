date: 2023-03-21
myo-type: porcine-beta-cardiac
myo-concentration: 0.5-ug/mL
conditions: 1uM-ATP_pCa-6.25
flowcell: 2
log:
    f2_s2:
        m1: 0
        m2: 1
        m3: 1
    f2_s3:
        m1: 1
        m2: 0
        m3: 1
        m4: 0
        m5: 1